package l3

import SymbolicCPSTreeModuleLow._

object CPSHoister extends (Tree => LetF) {
  def apply(tree: Tree): LetF = tree match {
    case LetP(name, prim, args, body) =>
      val LetF(funs, hoistedBody) = apply(body)
      LetF(funs, LetP(name, prim, args, hoistedBody))

    case LetC(cnts, body) =>
      val (newCnts, funsHoistedCnts) = cnts
        .map(cnt => (cnt, apply(cnt.body)))
        .map { case (cnt, LetF(funs, body)) => (Cnt(cnt.name, cnt.args, body), funs)}
        .unzip
      val LetF(funsHoistedBody, hoistedBody) = apply(body)

      val funsHoistedFlat = funsHoistedCnts.flatten ++ funsHoistedBody
      LetF(funsHoistedFlat, LetC(newCnts, hoistedBody))

    case LetF(funs, body) =>
      val (newFuns, funsHoistedFuns) = funs
        .map(fun => (fun, apply(fun.body)))
        .map { case (fun, LetF(funsHoisted, body)) =>
          (Fun(fun.name, fun.retC, fun.args, body), funsHoisted)
        }.unzip

      val LetF(funsHoistedBody, hoistedBody) = apply(body)
      val funsHoistedFlat = newFuns ++ funsHoistedFuns.flatten ++ funsHoistedBody
      LetF(funsHoistedFlat, hoistedBody)

    case e => LetF(Seq(), e)
  }
}
