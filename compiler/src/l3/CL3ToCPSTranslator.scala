package l3

import l3.{ L3Primitive => L3 }
import l3.{ SymbolicCL3TreeModule => S}
import l3.{ SymbolicCPSTreeModule => C}

object CL3ToCPSTranslator extends (S.Tree => C.Tree) {
  def apply(tree: S.Tree): C.Tree = nonTail(tree)(_ => 
      C.Halt(C.AtomL(IntLit(L3Int(0))))
    )

  def cond(condition: S.Tree)(thnCnt: C.Name, elsCnt: C.Name): C.Tree = {

    def transformTestPrim(p: L3TestPrimitive, args: Seq[S.Tree]): C.Tree = {
      def transformArgs(args: Seq[S.Tree])(atoms: Seq[C.Atom]): C.Tree = args match {
        case Nil => C.If(p, atoms, thnCnt, elsCnt)
        case x::xs =>
          nonTail(x)(v => transformArgs(xs)(atoms :+ v))
      }
      transformArgs(args)(Seq())
    }

    def litSelect[T](lit: CL3Literal, tru: T, fls: T): T = lit match {
      case BooleanLit(false) => fls
      case _ => tru
    }

    def singleLiteral(condE: S.Tree, argE: S.Tree, lit: CL3Literal, literalLeft: Boolean) = {
      val branchCntName = Symbol.fresh("ac")
      val literalCntName = litSelect(lit, thnCnt, elsCnt)
      val branchCnt = C.Cnt(branchCntName, Seq(), cond(argE)(thnCnt, elsCnt))

      val leftCnt = if (literalLeft) literalCntName else branchCntName
      val rightCnt = if (literalLeft) branchCntName else literalCntName

      C.LetC(Seq(branchCnt), cond(condE)(leftCnt, rightCnt))
    }

    condition match {
      case S.If(S.Lit(lit), e2, e3) => 
        cond(litSelect(lit, e2, e3))(thnCnt, elsCnt)
      case S.If(e1, S.Lit(lit2), S.Lit(lit3)) => 
        cond(e1)(litSelect(lit2, thnCnt, elsCnt), litSelect(lit3, thnCnt, elsCnt))
      case S.If(e1, e2, S.Lit(lit3)) => 
        singleLiteral(e1, e2, lit3, literalLeft=false)
      case S.If(e1, S.Lit(lit2), e3) => 
        singleLiteral(e1, e3, lit2, literalLeft=true)
      case S.Prim(p: L3TestPrimitive, primArgs) => 
        transformTestPrim(p, primArgs)
      case _ => {
        implicit val pos = condition.pos
        val primCond = S.Prim(L3.Eq, Seq(condition, S.Lit(BooleanLit(false))))
        cond(primCond)(elsCnt, thnCnt)
      }
    }
  }

  def tail(tree: S.Tree)(c: C.Name): C.Tree = {
    
    def tailApp(fun: S.Tree, args: Seq[S.Tree]) = {
      def tailFunArgs(args: Seq[S.Tree])(fId: C.Atom, ids: Seq[C.Atom]): C.Tree = args match {
        case Nil => C.AppF(fId, c, ids)
        case arg :: args => nonTail(arg)(v => tailFunArgs(args)(fId, ids :+ v))
      }
      
      nonTail(fun)(fId => tailFunArgs(args)(fId, Seq()))
    }
    
    def tailLetRec(funs: Seq[S.Fun], body: S.Tree) = {
      val cpsFuns = funs map { case S.Fun(name, args, body) =>
        val cName = Symbol.fresh("c-" + name)
        C.Fun(name, cName, args, tail(body)(cName))
      }

      C.LetF(cpsFuns, tail(body)(c))
    }

    tree match {
      case S.Let(bindings, body) => bindings.foldRight(tail(body)(c)) {
        case ((name, expr), tree) => nonTail(expr)(v => C.LetP(name, L3.Id, Seq(v), tree))
      }

      case S.If(condition, thn, els) => {
        val thnCntName = Symbol.fresh("ct")
        val thnCntBody = tail(thn)(c)
        val thnCnt = C.Cnt(thnCntName, Seq(), thnCntBody)

        val elsCntName = Symbol.fresh("cf")
        val elsCntBody = tail(els)(c)
        val elsCnt = C.Cnt(elsCntName, Seq(), elsCntBody)
        
        val ifBody = cond(condition)(thnCntName, elsCntName)
        
        C.LetC(Seq(thnCnt), C.LetC(Seq(elsCnt), ifBody))
      }

      case S.App(fun, args) => tailApp(fun, args)
      
      case S.LetRec(funs, body) => tailLetRec(funs, body)
      
      case S.Prim(p: L3TestPrimitive, args) => {
        implicit val pos = tree.pos
        val newPrim = S.If(tree, S.Lit(BooleanLit(true)), S.Lit(BooleanLit(false)))
        tail(newPrim)(c)
      }

      // The remaining cases (Lit, Ident, value Prim, Halt) should return to non-tail
      // since they all use the result value more than just applying the current continuation
      case _ => nonTail(tree)(v => C.AppC(c, Seq(v)))
    } 
  }
    
  
  def nonTail(tree: S.Tree)(ctx: C.Atom => C.Tree): C.Tree = {
    
    def transformApp(fun: S.Tree, args: Seq[S.Tree]) = {
      // Build return continuation
      val cntName = Symbol.fresh("c")
      val retName = Symbol.fresh("r")
      val returnCnt = C.Cnt(cntName, Seq(retName), ctx(C.AtomN(retName)))

      // Transforms each arg successively, accumulate generated atoms. At bottom create letc-apply
      // expression. Return resulting tree
      def transformFunArgs(args: Seq[S.Tree])(fId: C.Atom, ids: Seq[C.Atom]): C.Tree = args match {
        case Nil => C.LetC(Seq(returnCnt), C.AppF(fId, cntName, ids))
        case arg :: args => nonTail(arg)(v => transformFunArgs(args)(fId, ids :+ v))
      }
      
      // Transform initial function then start argument transformation
      nonTail(fun)(fId => transformFunArgs(args)(fId, Seq()))
    }

    def transformLetRec(funs: Seq[S.Fun], body: S.Tree) = {
      val cpsFuns = funs map { case S.Fun(name, args, body) =>
        val cName = Symbol.fresh("c-" + name)
        C.Fun(name, cName, args, tail(body)(cName))
      }

      C.LetF(cpsFuns, nonTail(body)(ctx))
    }

    def transformValPrim(p: L3ValuePrimitive, primArgs: Seq[S.Tree]) = {
      val name = Symbol.fresh("p")
      val body = ctx(C.AtomN(name))

      def transformArgs(args: Seq[S.Tree])(atoms: Seq[C.Atom]): C.Tree = args match {          
        case Nil =>
          // Finished gathering atoms, now we can just place them into the primitive
          C.LetP(name, p, atoms, body)
        case x :: xs =>
          // Transform the current argument and add it to the list
          nonTail(x)(v => transformArgs(xs)(atoms :+ v))
      }
      transformArgs(primArgs)(Seq())
    }

    tree match {
      case S.Lit(v) => ctx(C.AtomL(v))
      case S.Ident(id) => ctx(C.AtomN(id))
      case S.Let(bindings, body) => 
        bindings.foldRight(nonTail(body)(ctx)) { 
          case ((name, expr), tree) => nonTail(expr)(v => C.LetP(name, L3.Id, Seq(v), tree))
        }

      case S.If(condition, thn, els) => {
        val retCntName = Symbol.fresh("c")
        val retArgName = Symbol.fresh("r")
        val retCnt = C.Cnt(retCntName, Seq(retArgName), ctx(C.AtomN(retArgName)))

        val thnCntName = Symbol.fresh("ct")
        val thnCntBody = tail(thn)(retCntName)
        val thnCnt = C.Cnt(thnCntName, Seq(), thnCntBody)

        val elsCntName = Symbol.fresh("cf")
        val elsCntBody = tail(els)(retCntName)
        val elsCnt = C.Cnt(elsCntName, Seq(), elsCntBody)
        
        val ifBody = cond(condition)(thnCntName, elsCntName)
        
        C.LetC(Seq(retCnt), C.LetC(Seq(thnCnt), C.LetC(Seq(elsCnt), ifBody)))
      }


      case S.App(fun, args) => transformApp(fun, args)
      case S.LetRec(funs, body) => transformLetRec(funs, body)
      case S.Prim(p: L3TestPrimitive, args) => {
        implicit val pos = tree.pos
        val newPrim = S.If(tree, S.Lit(BooleanLit(true)), S.Lit(BooleanLit(false)))
        nonTail(newPrim)(ctx)
      }
      case S.Prim(p: L3ValuePrimitive, args) => transformValPrim(p, args)
      case S.Halt(arg) => nonTail(arg)(a => C.Halt(a))
    }
  }
    

}
